# Reference Software Requirements Ontology 
## 🚀 Goal
Reference Software Requirements Ontology (RSRO) aims at being a reference for software requirements notions. RSRO is centered in the conception of requirement as a goal to be achieved and addresses the distinction between functional and non-functional requirements, how requirements are documented in proper artifacts, among others

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Spring Boot 3.0
2. Spring Data Rest
3. Spring GraphQL


## 🔧 Install

1) Create a database with name rsro with **CREATE DATABASE rsro**.
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```
## 🔧 Usage

### Using Jar

```xml
<dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>rsro</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```

### Using Webservice

Execute docker-compose:
```bash 
docker-compose up -d 
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:](http://localhost:8081) to see Swagger
* Acess [http://localhost:/grapiql](http://localhost:8081/grapiql) to see Graphql.

## ✒️ Team

* **[paulossjunior@gmail.com](paulossjunior@gmail.com)**

## 📕 Literature

