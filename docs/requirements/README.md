# 📕Documentation: requirements

Define the requirements of a software project

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **RequirementArtifact** : -
* **RequirementDocument** : A Document composed of Requirements Artifacts that describe Requirements is said a Requirements Document (e.g., a Requirements Specification)
* **Requirement** : It is a goal to be achieved, representing a condition or capacity needed for the user (e.g., create service order
