package br.nemo.immigrant.ontology.service.rsro.requirements.controllers;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementArtifact;
import br.nemo.immigrant.ontology.entity.rsro.requirements.repositories.RequirementArtifactRepository;
import br.nemo.immigrant.ontology.service.rsro.requirements.records.RequirementArtifactInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class RequirementArtifactController  {

  @Autowired
  RequirementArtifactRepository repository;

  @QueryMapping
  public List<RequirementArtifact> findAllRequirementArtifacts() {
    return repository.findAll();
  }

  @QueryMapping
  public RequirementArtifact findByIDRequirementArtifact(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public RequirementArtifact createRequirementArtifact(@Argument RequirementArtifactInput input) {
    RequirementArtifact instance = RequirementArtifact.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public RequirementArtifact updateRequirementArtifact(@Argument Long id, @Argument RequirementArtifactInput input) {
    RequirementArtifact instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("RequirementArtifact not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteRequirementArtifact(@Argument Long id) {
    repository.deleteById(id);
  }

}
