package br.nemo.immigrant.ontology.service.rsro.requirements.repositories;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementArtifact;
import br.nemo.immigrant.ontology.entity.rsro.requirements.repositories.RequirementArtifactRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "requirementartifact", path = "requirementartifact")
public interface RequirementArtifactRepositoryWeb extends RequirementArtifactRepository {

}
