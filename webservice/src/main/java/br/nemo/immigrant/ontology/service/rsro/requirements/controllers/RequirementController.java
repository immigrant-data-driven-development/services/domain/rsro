package br.nemo.immigrant.ontology.service.rsro.requirements.controllers;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.Requirement;
import br.nemo.immigrant.ontology.entity.rsro.requirements.repositories.RequirementRepository;
import br.nemo.immigrant.ontology.service.rsro.requirements.records.RequirementInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class RequirementController  {

  @Autowired
  RequirementRepository repository;

  @QueryMapping
  public List<Requirement> findAllRequirements() {
    return repository.findAll();
  }

  @QueryMapping
  public Requirement findByIDRequirement(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Requirement createRequirement(@Argument RequirementInput input) {
    Requirement instance = Requirement.builder().name(input.name()).
                                                 description(input.description()).
                                                 internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Requirement updateRequirement(@Argument Long id, @Argument RequirementInput input) {
    Requirement instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Requirement not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteRequirement(@Argument Long id) {
    repository.deleteById(id);
  }

}
