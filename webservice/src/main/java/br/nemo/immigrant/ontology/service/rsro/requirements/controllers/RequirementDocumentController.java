package br.nemo.immigrant.ontology.service.rsro.requirements.controllers;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementDocument;
import br.nemo.immigrant.ontology.entity.rsro.requirements.repositories.RequirementDocumentRepository;
import br.nemo.immigrant.ontology.service.rsro.requirements.records.RequirementDocumentInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class RequirementDocumentController  {

  @Autowired
  RequirementDocumentRepository repository;

  @QueryMapping
  public List<RequirementDocument> findAllRequirementDocuments() {
    return repository.findAll();
  }

  @QueryMapping
  public RequirementDocument findByIDRequirementDocument(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public RequirementDocument createRequirementDocument(@Argument RequirementDocumentInput input) {
    RequirementDocument instance = RequirementDocument.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public RequirementDocument updateRequirementDocument(@Argument Long id, @Argument RequirementDocumentInput input) {
    RequirementDocument instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("RequirementDocument not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteRequirementDocument(@Argument Long id) {
    repository.deleteById(id);
  }

}
