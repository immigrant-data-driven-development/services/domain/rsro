package br.nemo.immigrant.ontology.service.rsro.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.nemo.immigrant.ontology.service.*"})
@EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
@EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.service.*"})
@OpenAPIDefinition(info = @Info(
  title = "Reference Software Requirements Ontology WebService",
  version = "1.0",
  description = "Reference Software Requirements Ontology (RSRO) aims at being a reference for software requirements notions. RSRO is centered in the conception of requirement as a goal to be achieved and addresses the distinction between functional and non-functional requirements, how requirements are documented in proper artifacts, among others"))

public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
