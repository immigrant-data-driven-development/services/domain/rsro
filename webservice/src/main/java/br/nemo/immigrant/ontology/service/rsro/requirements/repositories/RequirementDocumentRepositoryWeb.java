package br.nemo.immigrant.ontology.service.rsro.requirements.repositories;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementDocument;
import br.nemo.immigrant.ontology.entity.rsro.requirements.repositories.RequirementDocumentRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "requirementdocument", path = "requirementdocument")
public interface RequirementDocumentRepositoryWeb extends RequirementDocumentRepository {

}
