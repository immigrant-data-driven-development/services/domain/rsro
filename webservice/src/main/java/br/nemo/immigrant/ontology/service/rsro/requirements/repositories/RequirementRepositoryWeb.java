package br.nemo.immigrant.ontology.service.rsro.requirements.repositories;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.Requirement;
import br.nemo.immigrant.ontology.entity.rsro.requirements.repositories.RequirementRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "requirement", path = "requirement")
public interface RequirementRepositoryWeb extends RequirementRepository {

}
