package br.nemo.immigrant.ontology.entity.rsro.requirements.repositories;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.Requirement;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface RequirementRepository extends PagingAndSortingRepository<Requirement, Long>, ListCrudRepository<Requirement, Long> {

}
