package br.nemo.immigrant.ontology.entity.rsro.requirements.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.base.models.CommonConcept;


@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "requirement")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Requirement  extends CommonConcept implements Serializable {



  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "requirement")
  @Builder.Default
  private RequirementArtifact requirementartifact = null;


  @Builder.Default
  @Enumerated(EnumType.STRING)
  private RequirementType type = RequirementType.FUNCTIONAL;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Requirement elem = (Requirement) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Requirement {" +
         "id="+this.id+
          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", internalId='"+this.internalId+"'"+
          ", type='"+this.type+"'"+
      '}';
  }
}
