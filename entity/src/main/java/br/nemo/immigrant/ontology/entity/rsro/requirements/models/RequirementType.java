package br.nemo.immigrant.ontology.entity.rsro.requirements.models;

public enum RequirementType {
    FUNCTIONAL,
    NONFUNCTIONAL
}