package br.nemo.immigrant.ontology.entity.rsro.requirements.repositories;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface RequirementArtifactRepository extends PagingAndSortingRepository<RequirementArtifact, Long>, ListCrudRepository<RequirementArtifact, Long> {

}
