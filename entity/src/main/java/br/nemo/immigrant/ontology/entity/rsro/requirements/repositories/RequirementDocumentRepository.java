package br.nemo.immigrant.ontology.entity.rsro.requirements.repositories;

import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementDocument;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface RequirementDocumentRepository extends PagingAndSortingRepository<RequirementDocument, Long>, ListCrudRepository<RequirementDocument, Long> {

}
